<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $poneys;    

     public function setUp(){
        $this->poneys = new Poneys();
        $this->poneys->setCount(8);

      }

    public function test_removePoneyFromField() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->poneys->getCount());

    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage  Impossible
     */
    public function test2_removePoneyFromField() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->poneys->removePoneyFromField(9);
      
      // Assert
      $this->assertEquals(9, $this->poneys->getCount());


    }

     public function addDataProvider() {
        return array(
            array(1,7),
            array(2,6),
            array(3,5),
        );
    }
 
    /**
     *
     * @dataProvider addDataProvider
     */
    public function test3_removePoneyFromField($a, $expected) {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->poneys->removePoneyFromField($a);
      
      // Assert
      $this->assertEquals($expected, $this->poneys->getCount());

    }




    public function test_addPoney() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->poneys->addPoney();
      
      // Assert
      $this->assertEquals(9, $this->poneys->getCount());
    }
    

    public function test_getNames(){

      $this->poneys = $this ->getMockBuilder('Poneys')-> getMock();
      $this->poneys
           ->expects($this->exactly(1))
           ->method('getNames')
           ->willReturn(
            array('Joe','william','Jack','awerell')
          );
      $this->assertEquals(
          array('Joe','william','Jack','awerell'),$this->poneys->getNames());  

    }
    

    public function tearDown (){
      unset($this->poneys);
    }



  }
 ?>
